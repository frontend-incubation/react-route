import React, { useState } from "react";
import { useParams } from "react-router-dom";

import Nav from "./Nav";

export default function Article() {
  const [article, setArticle] = useState(null)
  const { topicId } = useParams()

  return (
    <>
      <Nav />
      <h1>This article is about {topicId}.</h1>
    </>
  )
}
