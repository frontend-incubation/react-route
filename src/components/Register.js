import React from "react";
import { useNavigate } from "react-router-dom";
import Nav from "./Nav";

export default function Register() {
  const navigate = useNavigate()

  return (
    <>
      <Nav />
      <h1>I'm at register</h1>
      <button onClick={() => navigate('/dashboard')}>Register</button>
    </>
  )
}
