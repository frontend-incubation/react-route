import React from "react";
import Nav from "./Nav";

export default function Home() {
  return (
    <>
      <Nav />
      <h1>I'm at home</h1>
    </>
  )
}
