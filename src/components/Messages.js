import React from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import Nav from "./Nav";

export default function Messages() {
  return (
    <>
      <Nav />
      <h1>I'm at Messages</h1>

      <Outlet />
    </>
  )
}
