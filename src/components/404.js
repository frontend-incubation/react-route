import React from "react";
import Nav from "./Nav";

export default function NotFound() {
  return (
    <>
      <Nav />
      <h1>404: Not Found</h1>
    </>
  )
}
