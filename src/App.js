import { Route, Routes } from 'react-router-dom';
import './App.css';
import Home from './components/Home';
import About from './components/About';
import Messages from './components/Messages';
import Settings from './components/Settings';
import NotFound from './components/404';
import Article from './components/Article';
import Chat from "./components/Chat";
import Register from './components/Register';
import Dashboard from './components/Dashboard';

function App() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/about" element={<About />} />
      <Route path="/register" element={<Register />} />
      <Route path="/dashboard" element={<Dashboard />} />
      <Route path="/messages" element={<Messages />}>
        <Route path=":chatId" element={<Chat />} />
      </Route>
      <Route path="/settings" element={<Settings />} />
      <Route path="/wiki/:topicId" element={<Article />} />
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
}

export default App;
