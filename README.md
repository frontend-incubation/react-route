# Getting Started with React Router

This repo demonstrates:

1. Basic React Router using `BrowserRouter`.
2. Route setup with `Route` and `Routes`.
3. Dealing with URL Parameters.
4. Nested Routes
